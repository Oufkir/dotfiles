# Dotfiles

This is my dotfiles for my manjaro install.

## Dependencies
 - i3-gaps
 - i3blocks
 - dmenu
 - urxvt
 - bdf-curie
 - zsh
 - python
 - oomox
 - pywal
 - perl